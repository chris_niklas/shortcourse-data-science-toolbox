TOPTARGETS := all clean

ZENODO_ID := 3380251

SUBDIRS := $(wildcard day-*/.)

$(TOPTARGETS): prerequisites $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: $(TOPTARGETS) $(SUBDIRS)



resources/service-account.json:
	(cd resources; ./decrypt.R $(ID_RSA) $(PASS_SERVICE_ACCOUNT))

resources/data/tbl_wb_hnp.csv:
	mkdir -p resources/data
	Rscript -e "download.file('https://zenodo.org/record/$(ZENODO_ID)/files/tbl_wb_hnp.tar.gz?download=1', 'resources/data/tbl_wb_hnp.tar.gz')"
	Rscript -e "untar('resources/data/tbl_wb_hnp.tar.gz', exdir = 'resources')"

resources/data/messy-data-1.sas7bdat:
	mkdir -p resources/data
	Rscript -e "download.file('https://zenodo.org/record/$(ZENODO_ID)/files/messy-data-1.sas7bdat?download=1', 'resources/data/messy-data-1.sas7bdat')"

resources/data/messy-data-2.csv:
	mkdir -p resources/data
	Rscript -e "download.file('https://zenodo.org/record/$(ZENODO_ID)/files/messy-data-2.csv?download=1', 'resources/data/messy-data-2.csv')"

prerequisites: resources/service-account.json resources/data/tbl_wb_hnp.csv resources/data/messy-data-2.csv resources/data/messy-data-1.sas7bdat




